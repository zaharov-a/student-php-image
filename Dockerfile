FROM php:5.6-apache
ENV TZ=Europe/Moscow

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN printf '[PHP]\ndate.timezone = "${TZ}"\n' > /usr/local/etc/php/conf.d/tzone.ini

RUN echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list

RUN apt-get update && \
   apt-get install -y \
        zlib1g-dev libpng-dev libjpeg-dev libfreetype6-dev \
        libcurl4-gnutls-dev \
        libxml2-dev

# openssl идет в коробке
#RUN docker-php-ext-install openssl

RUN docker-php-ext-install mysql mysqli pdo pdo_mysql
RUN docker-php-ext-install mbstring
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install zip
RUN docker-php-ext-install xml
RUN docker-php-ext-install curl
RUN docker-php-ext-install iconv
RUN pecl install redis-4.3.0
RUN docker-php-ext-enable redis

FROM php:5.6-apache

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN printf '[PHP]\ndate.timezone = "${TZ}"\n' > /usr/local/etc/php/conf.d/tzone.ini

RUN echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list

COPY --from=0 /usr/local/etc/php/conf.d /usr/local/etc/php/conf.d
COPY --from=0 /usr/local/lib/php/extensions/ /usr/local/lib/php/extensions/

RUN apt update && \
    apt install -y libjpeg62-turbo libpng16-16 libfreetype6 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/*

RUN a2enmod rewrite
RUN a2enmod remoteip
RUN service apache2 restart

#docker buildx build \
 #--push \
 #--platform linux/arm/v7,linux/arm64/v8,linux/amd64 \ --tag your-username/multiarch-example:buildx-latest .
#https://github.com/phusion/passenger-docker/issues/235#issuecomment-636318827
